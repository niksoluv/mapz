
#pragma once
#include "Source.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iterator>
#include <msclr\marshal_cppstd.h>
#include <tchar.h>

using namespace std;

vector<string> keywords{ "var", "if" };

vector<string> functions{ "moveMouse", "pushButtons" };


namespace Language2 {



	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace msclr::interop;
	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{

		

		void displayCode(vector<vector<string>> parcedCode) {
			string temp;
			bool fl = true;
			for (int i = 0; i < parcedCode.size(); ++i) {
				for (int j = 0; j < parcedCode[i].size(); ++j) {

					temp = parcedCode[i][j];
					Output->Text = Output->Text + "[" +
						marshal_as<String^>(temp) + "]" + "\t" + "->" + "\t";
					fl = true;
					for (int k = 0; k < keywords.size(); ++k) {
						if (temp == keywords[k]) {
							Output->Text = Output->Text + "keyword";
							fl = false;
							break;
						}
					}

					for (int k = 0; k < functions.size(); ++k) {
						if (temp == functions[k]) {
							Output->Text = Output->Text + "function";
							fl = false;
							break;
						}
					}

					if (fl) {
						if (temp == "(") {
							Output->Text = Output->Text + "open bracket";
						}

						else if (temp == ")") {
							Output->Text = Output->Text + "close bracket";
						}

						else if (temp == "{") {
							Output->Text = Output->Text + "open figure bracket";
						}

						else if (temp == "}") {
							Output->Text = Output->Text + "close figure bracket";
						}

						else if (temp == "=") {
							Output->Text = Output->Text + "equals";
						}

						else if (temp == ",") {
							Output->Text = Output->Text + "coma";
						}

						else if (temp == "+") {
							Output->Text = Output->Text + "plus";
						}

						else if (temp == "-") {
							Output->Text = Output->Text + "minus";
						}

						else if (temp == "/") {
							Output->Text = Output->Text + "divide";
						}

						else if (temp == "*") {
							Output->Text = Output->Text + "multiply";
						}

						else if (temp[0] == '"') {
							Output->Text = Output->Text + "string variable";
						}

						else if (temp == ">") {
							Output->Text = Output->Text + "greater";
						}

						else if (temp == "<") {
							Output->Text = Output->Text + "lower";
						}

						else {
							Output->Text = Output->Text + "variable";
						}
					}
					temp.clear();
					Output->Text = Output->Text + "\r\n\r\n";
				}
			}
		}

	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ Input;
private: System::Windows::Forms::Button^ button1;

public: System::Windows::Forms::TextBox^ Output;
private:
	protected:

	protected:



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->Input = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->Output = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// Input
			// 
			this->Input->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->Input->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Input->ForeColor = System::Drawing::Color::Lime;
			this->Input->Location = System::Drawing::Point(1, 3);
			this->Input->Multiline = true;
			this->Input->Name = L"Input";
			this->Input->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->Input->Size = System::Drawing::Size(401, 482);
			this->Input->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(1, 491);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Run";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// Output
			// 
			this->Output->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->Output->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Output->ForeColor = System::Drawing::Color::Lime;
			this->Output->Location = System::Drawing::Point(408, 3);
			this->Output->Multiline = true;
			this->Output->Name = L"Output";
			this->Output->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->Output->Size = System::Drawing::Size(401, 482);
			this->Output->TabIndex = 2;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ControlDark;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(813, 517);
			this->Controls->Add(this->Output);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->Input);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		Output->Text = "";
		String^ code = Input->Text;
		File::WriteAllText("sourceCode.txt", code);

		ifstream fin;
		fin.open("sourceCode.txt");
		string tmp;
		vector<string> sourceCode;
		while (!fin.eof()) {
			getline(fin, tmp);
			if (tmp != "") {
				deleteBadSpaces(tmp);
				sourceCode.push_back(tmp);
			}
		}
		fin.close();
		vector< vector<string> > parcedCode = parceCode(sourceCode);
		
		Execute exec(parcedCode);

		fin.open("Out.txt");
		string tmp2;
		vector<string> sourceCode2;
		Output->Text = "";
		while (!fin.eof()) {
			getline(fin, tmp2);
			Output->Text = Output->Text + marshal_as<String^>(tmp2) + "\r\n";
		}

		fin.close();
		ofstream fout;
		fout.open("Out.txt");
		fout << "";
		fout.close();

		/*sourceCode.clear();
		sourceCode2.clear();*/
		//fout.open("temp.txt");
		
	}
	};
	
}
