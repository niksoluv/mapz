#include "Source.h"



vector<Variable> variableTable;

void deleteBadSpaces(string& str) {
	/*for (std::string::iterator it = str.begin(); it != str.end(); it++)
	{
		std::string::iterator begin = it;
		while (it != str.end() && ::isspace(*it))
			it++;
		if (it - begin > 1)
			it = str.erase(begin + 1, it) - 1;
	}
	while (str[0] == ' ')
		str.erase(0, 1);
	while (str[str.size() - 1] == ' ')
		str.erase(str.size() - 1, 1);*/
	for (int i = 0; i < str.size(); ++i) {
		while(str[0]==' ')
			str.erase(0, 1);
		while (str[str.size()-1] == ' ')
			str.erase(str.size() - 1, 1);
		if (str[i] == ' ' && str[i + 1] == ' ')
			str.erase(i);
	}
}

//void GetDesktopResolution(int& horizontal, int& vertical)
//{
//	RECT desktop;
//	// Get a handle to the desktop window
//	const HWND hDesktop = GetDesktopWindow();
//	// Get the size of screen to the variable desktop
//	GetWindowRect(hDesktop, &desktop);
//	// The top left corner will have coordinates (0,0)
//	// and the bottom right corner will have coordinates
//	// (horizontal, vertical)
//	horizontal = desktop.right;
//	vertical = desktop.bottom;
//}

vector< vector<string> > parceCode(vector<string> src) {
	vector< vector<string> > res;
	vector<string> tmp;
	string delimiter = " ";
	size_t pos = 0;
	string token;
	for (int i = 0; i < src.size(); ++i) {
		while ((pos = src[i].find(delimiter)) != std::string::npos) {
			token = src[i].substr(0, pos);
			tmp.push_back(token);
			src[i].erase(0, pos + delimiter.length());
		}
		tmp.push_back(src[i]);
		res.push_back(tmp);
		tmp.clear();
	}
	return res;
}


Variable::Variable() {

}

Variable::Variable(string id, string val) {
	this->val = val;
	this->id = id;
}

string Variable::getId() {
	return this->id;
}

string Variable::getVal() {
	return this->val;
}

void Variable::setVal(string s) {
	this->val = s;
}

Expression::Expression() {

}

Expression::Expression(vector<string> &str) {
	double res = 0;
	bool changed = false;
	for (int i = 0; i < str.size(); ++i) {		
		
		for (int j = 0; j < operations.size(); ++j) {
			if (str[i] == operations[j]) {
				double first, second;
				if (isdigit(str[i - 1][0]))
					first = stod(str[i - 1]);
				if (i + 1 <= str.size())
					if (isdigit(str[i + 1][0]))
					second = stod(str[i + 1]);
				else
					second = 0;
				for (int k = 0; k < variableTable.size(); ++k) {
					if (variableTable[k].getId() == str[i - 1])
						first = stod(variableTable[k].getVal());
					if (variableTable[k].getId() == str[i + 1])
						second = stod(variableTable[k].getVal());
				}
				string op = operations[j];
				if (op == "+") {
					res = first + second;
					ofstream fout;
					fout.open("temp.txt");
					fout << res;
					fout.close();
					str.erase(str.begin() + i);
					str.erase(str.begin() + i);
					str[i - 1] = to_string(res);
					--j;
					--i;
				}
				if (op == "-") {
					res = first - second;
					ofstream fout;
					fout.open("temp.txt");
					fout << res;
					fout.close();
					str.erase(str.begin() + i);
					str.erase(str.begin() + i);
					str[i - 1] = to_string(res);
					--j;
					--i;
				}
				if (op == "*") {
					res = first * second;
					ofstream fout;
					fout.open("temp.txt");
					fout << res;
					fout.close();
					str.erase(str.begin() + i);
					str.erase(str.begin() + i);
					str[i - 1] = to_string(res);
					--j;
					--i;
				}
				if (op == "/") {
					res = first / second;
					ofstream fout;
					fout.open("temp.txt");
					fout << res;
					fout.close();
					str.erase(str.begin() + i);
					str.erase(str.begin() + i);
					str[i - 1] = to_string(res);
					--j;
					--i;
				}
				Variable tmp(str[1], str[3]);
				bool fl = false;
				for (int i = 0; i < variableTable.size(); ++i) {
					if (tmp.getId() == variableTable[i].getId())
						fl = true;
				}
				if (!fl)
					variableTable.push_back(tmp);
			}
			//break;
		}

		
	}
	for (int i = 0; i < str.size(); ++i) {

		if (str[i] == "=") {
			for (int n = 0; n < variableTable.size(); ++n) {
				if (str[i - 1] == variableTable[n].getId()) {
					if (isdigit(str[i + 1][0])) {
						variableTable[n].setVal(str[i + 1]);
						changed = true;
						break;
					}
					else {
						for (int m = 0; m < variableTable.size(); ++m) {
							if (str[i + 1] == variableTable[m].getId()) {
								variableTable[n].setVal(variableTable[m].getVal());
								changed = true;
								break;
							}
						}
					}
				}
				
			}
			if (!changed) {
				if (variableTable.size() > 0) {
					for (int n = 0; n < variableTable.size(); ++n) {
						if (str[i + 1] == variableTable[n].getId()) {
							Variable tmp(str[i - 1], variableTable[n].getVal());
							variableTable.push_back(tmp);
							break;
						}
						else {
							Variable tmp(str[i - 1], str[i + 1]);
							variableTable.push_back(tmp);
							break;
						}

					}
				}
				else {
					Variable tmp(str[i - 1], str[i + 1]);
					variableTable.push_back(tmp);
				}
				/*Variable tmp(str[i - 1], str[i + 1]);
				variableTable.push_back(tmp);*/
			}
		}

	}
	
}

Print::Print() {
	ofstream fout;
	fout.open("temp.txt");
	for (int i = 0; i < variableTable.size(); ++i) {
		fout << variableTable[i].getId() << "\t" << variableTable[i].getVal() << endl;
	}
	fout.close();
}

Print::Print(string s) {
	ofstream fout;
	fout.open("Out.txt", ios_base::app);
	if (variableTable.size() > 0) {
		for (int i = 0; i < variableTable.size(); ++i) {
			if (variableTable[i].getId() == s) {
				fout << variableTable[i].getVal() << endl;
			}
			else {
				if (i == variableTable.size() - 1)
					fout << s;
			}
		}
	}
	else {
		fout << s << endl  ;
	}
	fout.close();
}

Condition::Condition() {

}

Condition::Condition(vector<vector<string>> conditionCode) {
	ofstream fout;
	fout.open("cond.txt");
	for (int i = 0; i < conditionCode.size(); ++i) {
		for (int j = 0; j < conditionCode[i].size(); ++j) {
			fout << conditionCode[i][j] << " ";
		}
		fout << endl;
	}
	fout.close();
	for (int i = 0; i < conditionCode.size(); ++i) {
		string var1, var2;
		char operation;
		if (conditionCode[i][0] == "if") {
			var1 = conditionCode[i][2];
			operation = conditionCode[i][3][0];
			var2 = conditionCode[i][4];
			double firstOperator = 0, secondOperator = 0;
			if (isdigit(var1[0]))
				firstOperator = stod(var1);
			if (isdigit(var2[0]))
				secondOperator = stod(var2);
			switch (operation)
			{
			case '>':
				for (int k = 0; k < variableTable.size(); ++k) {
					if (var1 == variableTable[k].getId()) {
						firstOperator = stod(variableTable[k].getVal());
					}
					/*else {
						if (k == variableTable.size() - 1)
							firstOperator = stod(var1);
					}*/
					if (var2 == variableTable[k].getId()) {
						secondOperator = stod(variableTable[k].getVal());
					}
					/*else if (k == variableTable.size() - 1) {
						secondOperator = stod(var2);
					}*/
				}
				if (firstOperator > secondOperator) {
					vector<vector<string>> exec;
					for (int m = 1; m < conditionCode.size(); ++m) {
						if (conditionCode[m][0] != "}")
							exec.push_back(conditionCode[m]);
					}
					Execute ex(exec);
				}
				break;
			case '<':
				for (int k = 0; k < variableTable.size(); ++k) {
					if (var1 == variableTable[k].getId()) {
						firstOperator = stod(variableTable[k].getVal());
					}
					/*else {
						if (k == variableTable.size() - 1)
							firstOperator = stod(var1);
					}*/
					if (var2 == variableTable[k].getId()) {
						secondOperator = stod(variableTable[k].getVal());
					}
					/*else if (k == variableTable.size() - 1) {
						secondOperator = stod(var2);
					}*/
				}
				if (firstOperator < secondOperator) {
					vector<vector<string>> exec;
					for (int m = 1; m < conditionCode.size(); ++m) {
						if (conditionCode[m][0] != "}")
							exec.push_back(conditionCode[m]);
					}
					Execute ex(exec);
				}
			default:
				if (conditionCode[i][3] == "==") {
					for (int k = 0; k < variableTable.size(); ++k) {
						if (var1 == variableTable[k].getId()) {
							firstOperator = stod(variableTable[k].getVal());
						}
						/*else {
							if (k == variableTable.size() - 1)
								firstOperator = stod(var1);
						}*/
						if (var2 == variableTable[k].getId()) {
							secondOperator = stod(variableTable[k].getVal());
						}
						/*else if (k == variableTable.size() - 1) {
							secondOperator = stod(var2);
						}*/
					}
					if (firstOperator == secondOperator) {
						vector<vector<string>> exec;
						for (int m = 1; m < conditionCode.size(); ++m) {
							if (conditionCode[m][0] != "}")
								exec.push_back(conditionCode[m]);
						}
						Execute ex(exec);
					}
				}
				break;
			}
		}
		else if (conditionCode[i][0] == "while") {

		}
		else {

		}

	}
}

Execute::Execute() {

}

Execute::Execute(vector< vector<string> > parcedCode) {
	vector<vector<string>> conditionCode;
	int conditionBegin = 0, conditionEnd = 0, bracketCount = 0;
	bool bracketFind = false;
	for (int i = 0; i < parcedCode.size(); ++i) {
		for (int j = 0; j < parcedCode[i].size(); ++j) {
			string tmp2 = "";
			if (parcedCode[i][j] == "print") {
				j += 2;
				while (parcedCode[i][j] != ")") {
					tmp2 += parcedCode[i][j];
					if (parcedCode[i][j + 1] != ")")
						tmp2 += " ";
					j++;
				}
				Print a(tmp2);
				tmp2.clear();
			}

			else if (parcedCode[i][j] == "while" || parcedCode[i][j] == "for" || parcedCode[i][j] == "if") {


				conditionCode.clear();
				conditionBegin = i;

				while (parcedCode[i][j] != "}") {
					if (parcedCode[i][j] == "{") {
						bracketCount++;
						if (!bracketFind)
							bracketFind = true;
					}
					if (parcedCode[i][j] == "}")
						bracketCount--;

					conditionCode.push_back(parcedCode[i]);
					i++;
					if (bracketFind && bracketCount == 0) {

						break;
					}
				}
				conditionCode.push_back(parcedCode[i]);
				Condition a(conditionCode);
			}
			else if (parcedCode[i][j] == "moveMouse" || parcedCode[i][j] == "readAndPlay"
				|| parcedCode[i][j] == "pushButtons") {
				Func a(parcedCode[i]);
			}
			else {
				Expression Line(parcedCode[i]);
				break;
			}
		}
	}
	ofstream fout;
	fout.open("variables.txt");
	for (int i = 0; i < variableTable.size(); ++i) {
		fout << variableTable[i].getId() << "\t" << variableTable[i].getVal() << endl;
	}
	fout.close();
}

Func::Func() {

}

Func::Func(vector<string> str) {
	
	if (str[0] == "moveMouse") {
		string x = str[2], y = str[4];
		for (int k = 0; k < variableTable.size(); ++k) {
			if (x == variableTable[k].getId()) {
				x = variableTable[k].getVal();
			}
			if (y == variableTable[k].getId()) {
				y = variableTable[k].getVal();
			}
		}
		Func a;
		a.moveMouse(x, y);
	}
	else if (str[0] == "readAndPlay") {
		string time = str[2];
		for (int k = 0; k < variableTable.size(); ++k) {
			if (time == variableTable[k].getId()) {
				time = variableTable[k].getVal();
			}
		}
		Func a;
		a.readAndPlay(time);
	}
	else if (str[0] == "pushButtons") {
		string s = str[2];
		for (int k = 0; k < variableTable.size(); ++k) {
			if (s == variableTable[k].getId()) {
				s = variableTable[k].getVal();
			}
		}
		Func a;
		a.pushButtons(s);
	}
}

void Func::moveMouse(string x, string y) {

	mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE,
		(int)stod(x) * (65535 / 1366),
		(int)stod(y) * (65535 / 768), 0, 0);
}
void Func::readAndPlay(string time) {
	Sleep(2000);
	int start = clock();
	int duration = (int)stod(time);
	tagPOINT p;
	vector<int> posx;
	vector<int> posy;
	ofstream fout;
	fout.open("pos.txt");
	while ((clock() - start) < duration) {
		Sleep(1);
		GetCursorPos(&p);
		posx.push_back(p.x);
		fout << "x = " << p.x << "\t";
		posy.push_back(p.y);
		fout << "y = " << p.y << "\n";
	}
	for (int i = 0; i < posx.size(); ++i) {
		Sleep(10);
		mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE,
			posx[i] * (65535.0f / horizontal),
			posy[i] * (65535.0f / vertical), 0, 0);
	}
}

void Func::pushButtons(string str) {
	Sleep(2000);
	for (int i = 0; i < str.size(); ++i) {
		Sleep(1000);
		INPUT input;
		WORD vkey = str[i]; // see link below
		input.type = INPUT_KEYBOARD;
		input.ki.wScan = MapVirtualKey(vkey, MAPVK_VK_TO_VSC);
		input.ki.time = 0;
		input.ki.dwExtraInfo = 0;
		input.ki.wVk = vkey;
		input.ki.dwFlags = 0; // there is no KEYEVENTF_KEYDOWN
		SendInput(1, &input, sizeof(INPUT));

		input.ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, &input, sizeof(INPUT));
	}
}