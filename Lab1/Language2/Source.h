#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iterator>
#include <windows.h>
#include <process.h>
#include "wtypes.h"
#include <ctime>

#define horizontal 1366
#define vertical 768

using namespace std;

void deleteBadSpaces(string& str);

vector<vector<string>> parceCode(vector<string> src);

void displayCode(vector<vector<string>>);

static vector<string> operations{ "+", "-", "*", "/", "%" };

//void GetDesktopResolution(int& horizontal, int& vertical);

class Variable {
private:
	string id;
	string val;
public:
	Variable();
	Variable(string val, string id);
	string getVal();
	string getId();
	void setVal(string);
};

class Expression {
public:
	Expression();
	Expression(vector<string> &str);
};

class Print {
public:
	Print();
	Print(string);
	//Print(Variable);
};

class Condition {
public:
	Condition();
	Condition(vector<vector<string>>);
};

class Execute {
public:
	Execute();
	Execute(vector< vector<string> > parcedCode);
};

class Func {
public:
	Func();
	Func(vector<string>);
	void moveMouse(string x, string y);
	void readAndPlay(string time);
	void pushButtons(string str);
};