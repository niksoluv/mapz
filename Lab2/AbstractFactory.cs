﻿using myGamev1_0.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {

	public interface AF {
		Zombie getZombie(PictureBox pb);
		Skeleton getSkeleton(PictureBox pb);
	}
	public class bowFactory : AF {
		public Zombie getZombie(PictureBox pb) {
			pb.Image = Resources.bowz;
			return new bowZombie();
		}

		public Skeleton getSkeleton(PictureBox pb) {
			pb.Image = Resources.bows;
			return new bowSkeleton();
		}
	}

	public class swordFactory : AF {
		public Zombie getZombie(PictureBox pb) {
			pb.Image = Resources.zombie;
			return new swordZombie();
		}

		public Skeleton getSkeleton(PictureBox pb) {
			pb.Image = Resources.skeleton;
			return new swordSkeleton();
		}
	}

	public abstract class Zombie {
		public string name;
	}

	public class bowZombie : Zombie {
		public string name;
		public bowZombie() {
			this.name = "bowZombie";
			base.name = "bowZombie";
		}
	}

	public class swordZombie : Zombie {
		public string name;
		public swordZombie() {
			this.name = "swordZombie";
			base.name = "swordZombie";
		}
	}

	public abstract class Skeleton {
		public string name;
	}

	public class bowSkeleton : Skeleton {
		public string name;
		public bowSkeleton() {
			this.name = "bowSkeleton";
			base.name = "bowSkeleton";
		}
	}

	public class swordSkeleton : Skeleton {
		public string name;
		public swordSkeleton() {
			this.name = "swordSkeleton";
			base.name = "swordSkeleton";
		}
	}
}
