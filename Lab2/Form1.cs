﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	public partial class Form1 : Form {
		Pet Gpet = null;
		List<Weapon> weaponList = new List<Weapon>();
		List<Armour> armourList = new List<Armour>();
		List<Potion> potionList = new List<Potion>();
		public Form1() {
			InitializeComponent();
			MapLoaderSingleton loader = MapLoaderSingleton.getLoader();
			loader.loadMap(this);
			Pet pet = new Pet(petSprite);
			Gpet = pet;
		}

		private void button1_Click(object sender, EventArgs e) {
			
			ThreadSingleton ts = ThreadSingleton.getCounter();
			ts.decCount(textBox1);

			AF abstractFactory = new bowFactory();
			abstractFactory.getSkeleton(enemySprite);

			var rpet = new LegendaryPet(Gpet, petSprite);
			ShopFacade SF = new ShopFacade();
			

			weaponList.Add(SF.buyWeapon(100));
			weaponList.Add(SF.buyWeapon(1));
			weaponList.Add(SF.buyWeapon(100));

			armourList.Add(SF.buyArmour(1));
			armourList.Add(SF.buyArmour(100));
			armourList.Add(SF.buyArmour(1));

			for(int i = 0; i < armourList.Count; ++i) {
				shopWindow.Text = shopWindow.Text + " Armour cost: " + 
					Convert.ToString(armourList[i].cost) + "\tdmg: " +
					Convert.ToString(armourList[i].damagePoints) + "\tarmour: " +
					Convert.ToString(armourList[i].armourPoints) + "\r\n";
			}
			for (int i = 0; i < armourList.Count; ++i) {
				shopWindow.Text = shopWindow.Text + " Weapon cost: " +
					Convert.ToString(weaponList[i].cost) + "\tdmg: " +
					Convert.ToString(weaponList[i].damagePoints) + "\tarmour: " +
					Convert.ToString(weaponList[i].armourPoints) + "\r\n";
			}

			var healBuilder = new HealPotionBuilder();
			var damageBuilder = new DamagePotionBuilder();
			var shop = new buyPotion();

			shop.setPotionBuilder(healBuilder);
			shop.createPotion();

			potionList.Add(shop.GetPotion());
			shop.setPotionBuilder(damageBuilder);
			shop.createPotion();
			potionList.Add(shop.GetPotion());
			for (int i = 0; i < potionList.Count; ++i) {
				shopWindow.Text = shopWindow.Text + " Potion cost: " +
					Convert.ToString(potionList[i].cost) + "\tdmg: " +
					Convert.ToString(potionList[i].damagePoints) + "\theal: " +
					Convert.ToString(potionList[i].healPoints) + "\r\n";
			}
		}

		private void shop_Click(object sender, EventArgs e) {
			Form mod = new Shop();
			mod.Owner = this;
			mod.Show();
			this.Hide();
		}
	}
}
