﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	class MapLoaderSingleton {
		private MapLoaderSingleton() { }

		private static MapLoaderSingleton singletonInstance = null;

		public static MapLoaderSingleton getLoader() {
			if (singletonInstance == null)
				singletonInstance = new MapLoaderSingleton();
			return singletonInstance;
		}
		public void loadMap(Form Form1) {
			Form1.BackgroundImage = Properties.Resources.map;
			Form1.BackgroundImageLayout = ImageLayout.Stretch;
			
		}
	}
}
