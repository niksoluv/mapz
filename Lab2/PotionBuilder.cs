﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myGamev1_0 {
	public class Potion {
		public int healPoints = 0;
		public int damagePoints = 0;
		public int cost = 0;
	}

	public abstract class PotionBuilder {
		protected Potion potion { get; set; }

		public void createPotion() {
			potion = new Potion();
		}
		public Potion getPotion() {
			return potion;
		}

		public abstract void setHealPoints();
		public abstract void setDamagePoints();
		public abstract void setCost();
	}

	public class HealPotionBuilder : PotionBuilder {
		public override void setHealPoints() {
			potion.healPoints = 30;
		}
		public override void setDamagePoints() {
			potion.damagePoints = 0;
		}
		public override void setCost() {
			potion.cost = 20;
		}
	}

	public class DamagePotionBuilder : PotionBuilder {
		public override void setHealPoints() {
			potion.healPoints = 0;
		}
		public override void setDamagePoints() {
			potion.damagePoints = 4;
		}
		public override void setCost() {
			potion.cost = 35;
		}
	}

	class buyPotion {
		private PotionBuilder potionBuilder;

		public void setPotionBuilder(PotionBuilder _potionBuilder) {
			potionBuilder = _potionBuilder;
		}

		public Potion GetPotion() {
			return potionBuilder.getPotion();
		}

		public void createPotion() {
			potionBuilder.createPotion();
			potionBuilder.setHealPoints();
			potionBuilder.setDamagePoints();
			potionBuilder.setCost();
		}
	}
}
