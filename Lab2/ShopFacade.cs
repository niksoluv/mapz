﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	class ShopWeaponFacade {
		public Weapon buyWeapon() {
			Weapon wp = new Weapon();
			wp.armourPoints = 0;
			wp.damagePoints = 10;
			wp.cost = 100;
			return wp;
		}
		public Weapon buyLegendaryWeapon() {
			Weapon wp = new Weapon();
			wp.armourPoints = 0;
			wp.damagePoints = 30;
			wp.cost = 200;
			return wp;
		}
	}

	class ShopArmourFacade {
		public Armour buyArmour() {
			Armour ar = new Armour();
			ar.armourPoints = 20;
			ar.damagePoints = 0;
			ar.cost = 70;
			return ar;
		}
		public Armour buyLegendaryArmour() {
			Armour ar = new Armour();
			ar.armourPoints = 50;
			ar.damagePoints = 0;
			ar.cost = 140;
			return ar;
		}
	}

	class ShopFacade {
		private ShopArmourFacade shopArmourFacade = new ShopArmourFacade();
		private ShopWeaponFacade shopWeaponFacade = new ShopWeaponFacade();

		public Weapon buyWeapon(int damage) {
			Weapon wp = new Weapon();
			int weaponPrice = 0;
			if (damage > 20) {
				wp = shopWeaponFacade.buyWeapon();
				weaponPrice = wp.cost;
			}
			else {
				wp = shopWeaponFacade.buyLegendaryWeapon();
				weaponPrice = wp.cost;
			}
			return wp;
		}
		public Armour buyArmour(int armour) {
			int armourPrice = 0;
			Armour ar = new Armour();
			if (armour > 10) {
				ar = shopArmourFacade.buyLegendaryArmour();
				armourPrice = ar.cost;
			}

			else {
				ar = shopArmourFacade.buyArmour();
				armourPrice = ar.cost;
			}
			return ar;
		}
	}

	
}
