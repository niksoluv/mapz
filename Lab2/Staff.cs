﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myGamev1_0 {
	public class Weapon {
		public int damagePoints = 0;
		public int armourPoints = 0;
		public int cost = 0;
	}

	public class Armour {
		public int damagePoints = 0;
		public int armourPoints = 0;
		public int cost = 0;
	}
}
