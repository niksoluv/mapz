﻿namespace myGamev1_0 {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.Start = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.heroSprite = new System.Windows.Forms.PictureBox();
			this.enemySprite = new System.Windows.Forms.PictureBox();
			this.petSprite = new System.Windows.Forms.PictureBox();
			this.money = new System.Windows.Forms.TextBox();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.shop = new System.Windows.Forms.Button();
			this.shopWindow = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.heroSprite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.enemySprite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.petSprite)).BeginInit();
			this.SuspendLayout();
			// 
			// Start
			// 
			this.Start.Location = new System.Drawing.Point(713, 11);
			this.Start.Name = "Start";
			this.Start.Size = new System.Drawing.Size(75, 23);
			this.Start.TabIndex = 0;
			this.Start.Text = "Start";
			this.Start.UseVisualStyleBackColor = true;
			this.Start.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(13, 13);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "0";
			// 
			// heroSprite
			// 
			this.heroSprite.Image = global::myGamev1_0.Properties.Resources.hero;
			this.heroSprite.Location = new System.Drawing.Point(83, 152);
			this.heroSprite.Name = "heroSprite";
			this.heroSprite.Size = new System.Drawing.Size(143, 213);
			this.heroSprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.heroSprite.TabIndex = 2;
			this.heroSprite.TabStop = false;
			// 
			// enemySprite
			// 
			this.enemySprite.Location = new System.Drawing.Point(532, 152);
			this.enemySprite.Name = "enemySprite";
			this.enemySprite.Size = new System.Drawing.Size(159, 213);
			this.enemySprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.enemySprite.TabIndex = 3;
			this.enemySprite.TabStop = false;
			// 
			// petSprite
			// 
			this.petSprite.Location = new System.Drawing.Point(37, 342);
			this.petSprite.Name = "petSprite";
			this.petSprite.Size = new System.Drawing.Size(24, 23);
			this.petSprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.petSprite.TabIndex = 4;
			this.petSprite.TabStop = false;
			// 
			// money
			// 
			this.money.Location = new System.Drawing.Point(138, 13);
			this.money.Name = "money";
			this.money.Size = new System.Drawing.Size(100, 20);
			this.money.TabIndex = 5;
			this.money.Text = "1000";
			// 
			// shop
			// 
			this.shop.Location = new System.Drawing.Point(713, 40);
			this.shop.Name = "shop";
			this.shop.Size = new System.Drawing.Size(75, 23);
			this.shop.TabIndex = 6;
			this.shop.Text = "Shop";
			this.shop.UseVisualStyleBackColor = true;
			this.shop.Click += new System.EventHandler(this.shop_Click);
			// 
			// shopWindow
			// 
			this.shopWindow.Location = new System.Drawing.Point(13, 377);
			this.shopWindow.Multiline = true;
			this.shopWindow.Name = "shopWindow";
			this.shopWindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.shopWindow.Size = new System.Drawing.Size(775, 90);
			this.shopWindow.TabIndex = 7;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(800, 479);
			this.Controls.Add(this.shopWindow);
			this.Controls.Add(this.shop);
			this.Controls.Add(this.money);
			this.Controls.Add(this.petSprite);
			this.Controls.Add(this.enemySprite);
			this.Controls.Add(this.heroSprite);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.Start);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.heroSprite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.enemySprite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.petSprite)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button Start;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.PictureBox heroSprite;
		private System.Windows.Forms.PictureBox enemySprite;
		private System.Windows.Forms.PictureBox petSprite;
		private System.Windows.Forms.TextBox money;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Button shop;
		private System.Windows.Forms.TextBox shopWindow;
	}
}

