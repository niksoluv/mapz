﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	class ThreadSingleton {
		private ThreadSingleton() { }

		private int time = 20;
		private static ThreadSingleton clockInstance;
		private static readonly object locker = new object();

		public static ThreadSingleton getCounter() {
			lock (locker) {
				if(clockInstance == null)
					clockInstance = new ThreadSingleton();
			}
			return clockInstance;
		}

		public void decCount(TextBox tb) {
			time--;
			tb.Text = time.ToString();
		}
	}
}
