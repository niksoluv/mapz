﻿using myGamev1_0.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {

	public interface AF {
		Zombie getZombie(PictureBox pb);
		Skeleton getSkeleton(PictureBox pb);
	}
	public class bowFactory : AF {
		public Zombie getZombie(PictureBox pb) {
			
			return new bowZombie(pb);
		}

		public Skeleton getSkeleton(PictureBox pb) {
			
			return new bowSkeleton(pb);
		}
	}

	public class swordFactory : AF {
		public Zombie getZombie(PictureBox pb) {
			
			return new swordZombie(pb);
		}

		public Skeleton getSkeleton(PictureBox pb) {
			
			return new swordSkeleton(pb);
		}
	}

	public class Enemy {
		public int damagePoints = 0;
		public int armourPoints = 0;
		public int healthPoints = 0;
	}

	public class Zombie : Enemy {
	}

	public class bowZombie : Zombie {
		public bowZombie(PictureBox pb) {
			damagePoints = 1;
			armourPoints = 1;
			healthPoints = 1;
			pb.Image = Resources.bowz;
		}
	}

	public class swordZombie : Zombie {
		public swordZombie(PictureBox pb) {
			damagePoints = 2;
			armourPoints = 2;
			healthPoints = 2;
			pb.Image = Resources.zombie;
		}
	}

	public class Skeleton : Enemy {
	}

	public class bowSkeleton : Skeleton {
		public bowSkeleton(PictureBox pb) {
			damagePoints = 3;
			armourPoints = 3;
			healthPoints = 3;
			pb.Image = Resources.bows;
		}
	}

	public class swordSkeleton : Skeleton {
		public swordSkeleton(PictureBox pb) {
			damagePoints = 4;
			armourPoints = 4;
			healthPoints = 4;
			pb.Image = Resources.skeleton;
		}
	}
}
