﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	public interface ICommand {
		void Execute(Enemy enemy, TextBox tb, bool isFighting, Label l);
	}

	// Receiver - Получатель

	public class playerCommand : ICommand {
		User player;
		public playerCommand(User pl) {
			player = pl;
		}
		public void Execute(Enemy enemy, TextBox tb, bool isFighting, Label l) {
			player.dealDamage(enemy, tb, isFighting, l);
		}
	}

	// Invoker - инициатор
	public class Controller {
		ICommand command;

		public Controller() { }

		public void SetCommand(ICommand com) {
			command = com;
		}

		public void battle(Enemy enemy, TextBox tb, bool isFighting, Label l) {
			command.Execute(enemy, tb, isFighting, l);
		}
	}
}
