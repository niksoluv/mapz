﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	public partial class Form1 : Form {
		Pet Gpet = null;
		List<Weapon> weaponList = new List<Weapon>();
		List<Armour> armourList = new List<Armour>();
		List<Potion> potionList = new List<Potion>();
		AF abstractFactorySwords = new swordFactory();
		AF abstractFactoryBows = new bowFactory();
		User player = new User();
		Enemy enemy = new Enemy();
		bool isFighting = false;
		ThreadSingleton ts;
		ShopFacade SF = new ShopFacade();
		HealPotionBuilder healBuilder = new HealPotionBuilder();
		DamagePotionBuilder damageBuilder = new DamagePotionBuilder();
		GameHistory game = new GameHistory();
		Controller pult = new Controller();

		public Form1() {
			InitializeComponent();
			MapLoaderSingleton loader = MapLoaderSingleton.getLoader();
			loader.loadMap(this);
			Pet pet = new Pet(petSprite);
			Gpet = pet;
			movesLabel.Text = "15";
			moneyLabel.Text = "0";
			

			player.armourPoints = player.armourPoints +
				player.equipment.armour.armourPoints + player.equipment.weapon.armourPoints;
			player.damagePoints = player.damagePoints +
				player.equipment.weapon.damagePoints + player.equipment.armour.damagePoints;

			userAr.Text = Convert.ToString(0);
			userDMG.Text = Convert.ToString(0);
			userHP.Text = Convert.ToString(0);
			enemyAr.Text = Convert.ToString(0);
			enemyDMG.Text = Convert.ToString(0);
			enemyHP.Text = Convert.ToString(0);
		}

		private void button1_Click(object sender, EventArgs e) {

			if (!isFighting) {
				ts = ThreadSingleton.getCounter();
				ts.incCount(movesLabel);
				isFighting = true;
				Random r = new Random();
				//create enemy
				int en = r.Next(2);
				if (en == 1) {
					en = r.Next(2);
					if (en == 1) {
						enemy = abstractFactorySwords.getSkeleton(enemySprite);
					}
					else {
						enemy = abstractFactorySwords.getZombie(enemySprite);
					}
					enemy.armourPoints = player.armourPoints * r.Next(3);
					enemy.healthPoints = player.healthPoints * r.Next(3);
					enemy.damagePoints = player.damagePoints * r.Next(2);
					enemyHP.Text = Convert.ToString(enemy.healthPoints);
					enemyAr.Text = Convert.ToString(enemy.armourPoints);
					enemyDMG.Text = Convert.ToString(enemy.damagePoints);
				}
				else {
					en = r.Next(2);
					if (en == 1) {
						enemy = abstractFactoryBows.getSkeleton(enemySprite);
					}
					else {
						enemy = abstractFactoryBows.getZombie(enemySprite);
					}
					enemyHP.Text = Convert.ToString(enemy.healthPoints);
					enemyAr.Text = Convert.ToString(enemy.armourPoints);
					enemyDMG.Text = Convert.ToString(enemy.damagePoints);
				}
				//form update
				userHP.Text = Convert.ToString(player.healthPoints);
				userAr.Text = Convert.ToString(player.armourPoints);
				userDMG.Text = Convert.ToString(player.damagePoints);
				enemyHP.Text = Convert.ToString(enemy.healthPoints);
				enemyAr.Text = Convert.ToString(enemy.armourPoints);
				enemyDMG.Text = Convert.ToString(enemy.damagePoints);

				moneyLabel.Text = Convert.ToString(player.money);



				var rpet = new LegendaryPet(Gpet, petSprite);
				
			}
			else {
				Output.Text = Output.Text + "Youre battling now!!!" + "\r\n";
			}
		}

		private void shop_Click(object sender, EventArgs e) {
			var shop = new buyPotion();
			potionList.Clear();
			Random r = new Random();
			shop.setPotionBuilder(healBuilder);
			int n = r.Next(10);
			for (int i = 0; i < n; ++i) {
				shop.createPotion();
				potionList.Add(shop.GetPotion());
			}

			n = r.Next(10);
			shop.setPotionBuilder(damageBuilder);
			for (int i = 0; i < n; ++i) {
				shop.createPotion();
				potionList.Add(shop.GetPotion());
			}

			PotionLibrary lib = new PotionLibrary(potionList);
			Reader reader = new Reader();
			reader.SeePotions(lib, Output);

		}
		private void label1_Click(object sender, EventArgs e) {

		}

		private void label4_Click(object sender, EventArgs e) {

		}

		private void label5_Click(object sender, EventArgs e) {

		}

		private void button1_Click_1(object sender, EventArgs e) {
			//if (isFighting) {
			int enemyDP = enemy.damagePoints;
			int enemyAR = enemy.armourPoints;
			int enemyHPO = enemy.healthPoints;
			int playerDP = player.damagePoints;
			int playerAR = player.armourPoints;
			int playerHPO = player.healthPoints;

			//GO!
			pult.SetCommand(new playerCommand(player));
			pult.battle(enemy, Output, isFighting, moneyLabel);
			
			if (enemy.healthPoints <= 0) {
				isFighting = false;
				Output.Text = Output.Text + "Enemy ded\r\n";
				player.money += enemyDP + enemyHPO;
				Output.Text = Output.Text + "You earned " +
					Convert.ToString(enemyDP + enemyHPO) + " coins\r\n";
				moneyLabel.Text = Convert.ToString(player.money);
			}
			if (player.healthPoints <= 0) {
				isFighting = false;
				Output.Text = Output.Text + "You ded\r\n";
			}
			////form update

			userHP.Text = Convert.ToString(player.healthPoints);
			userAr.Text = Convert.ToString(player.armourPoints);
			userDMG.Text = Convert.ToString(player.damagePoints);
			enemyHP.Text = Convert.ToString(enemy.healthPoints);
			enemyAr.Text = Convert.ToString(enemy.armourPoints);
			enemyDMG.Text = Convert.ToString(enemy.damagePoints);

			ts = ThreadSingleton.getCounter();
			ts.decCount(movesLabel);
			

		}

		private void button2_Click(object sender, EventArgs e) {

			Output.Text = Output.Text + "Weapon : " +
				Convert.ToString(player.equipment.weapon.damagePoints) + " / " +
				Convert.ToString(player.equipment.weapon.armourPoints) + "\r\n";
			Output.Text = Output.Text + "Amrour : " +
				Convert.ToString(player.equipment.armour.damagePoints) + " / " +
				Convert.ToString(player.equipment.armour.armourPoints) + "\r\n";
		}

		private void Output_ChangeUICues(object sender, UICuesEventArgs e) {

		}

		private void Output_MultilineChanged(object sender, EventArgs e) {
			
		}

		private void Output_TextChanged(object sender, EventArgs e) {
			Output.SelectionStart = Output.Text.Length;
			Output.ScrollToCaret();
		}

		private void button4_Click(object sender, EventArgs e) {
			game.History.Push(player.SaveState());
			Output.Text = Output.Text + "Game saved!\r\n";
		}

		private void button5_Click(object sender, EventArgs e) {
			player.RestoreState(game.History.Pop());
			userAr.Text = Convert.ToString(player.armourPoints);
			userDMG.Text = Convert.ToString(player.damagePoints);
			userHP.Text = Convert.ToString(player.healthPoints);
			moneyLabel.Text = Convert.ToString(player.money);
			Output.Text = Output.Text + "CTRL+Z!\r\n";
		}
	}
}
