﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	class Reader {
		public void SeePotions(PotionLibrary library, TextBox tb) {
			PotionsIterator iterator = library.CreateNumerator();
			while (iterator.HasNext()) {
				Potion potion = iterator.Next();
				tb.Text = tb.Text + " Potion cost: " +
					Convert.ToString(potion.cost) + "\tdmg: " +
					Convert.ToString(potion.damagePoints) + "\theal: " +
					Convert.ToString(potion.healPoints) + "\r\n";
			}
		}
	}

	interface PotionsIterator {
		bool HasNext();
		Potion Next();
	}
	interface IBookNumerable {
		PotionsIterator CreateNumerator();
		int Count { get; }
		Potion this[int index] { get; }
	}

	class PotionLibrary : IBookNumerable {
		private List<Potion> potions;
		public PotionLibrary(List<Potion> pot) {
			potions = pot;
		}
		public int Count {
			get { return potions.Count; }
		}

		public Potion this[int index] {
			get { return potions[index]; }
		}
		public PotionsIterator CreateNumerator() {
			return new LibraryNumerator(this);
		}
	}
	class LibraryNumerator : PotionsIterator {
		IBookNumerable aggregate;
		int index = 0;
		public LibraryNumerator(IBookNumerable a) {
			aggregate = a;
		}
		public bool HasNext() {
			return index < aggregate.Count;
		}

		public Potion Next() {
			return aggregate[index++];
		}
	}
}
