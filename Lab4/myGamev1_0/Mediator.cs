﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
    abstract class Student {
        public abstract void Send(string msg, MediatorInterface colleague);
    }
    abstract class MediatorInterface {
        protected Student mediator;

        public MediatorInterface(Student mediator) {
            this.mediator = mediator;
        }

        public virtual void Send(string message) {
            mediator.Send(message, this);
        }
        public abstract void Notify(string message);
    }
    // класс заказчика
    class Boss : MediatorInterface {
        public Boss(Student mediator)
            : base(mediator) { }

        public override void Notify(string message) {
            Console.WriteLine("message to boss: " + message);
        }
    }
    // класс программиста
    class SmartStudent : MediatorInterface {
        public SmartStudent(Student mediator)
            : base(mediator) { }

        public override void Notify(string message) {
            Console.WriteLine("message to smart: " + message);
        }
    }
    // класс тестера
    class OrdinaryStudent : MediatorInterface {
        public OrdinaryStudent(Student mediator)
            : base(mediator) { }

        public override void Notify(string message) {
            Console.WriteLine("message to ord: " + message);
        }
    }

    class ManagerMediator : Student {
        public MediatorInterface boss { get; set; }
        public MediatorInterface smartStudent { get; set; }
        public MediatorInterface ordStudent { get; set; }
        public override void Send(string msg, MediatorInterface colleague) {
            // if message from boss
            if (boss == colleague)
                boss.Notify(msg);
            // if message from from smart
            else if (smartStudent == colleague)
                smartStudent.Notify(msg);
            // if message from ord
            else if (ordStudent == colleague)
                ordStudent.Notify(msg);
        }
    }
}
