﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myGamev1_0 {
    public class HeroMemento {
        public int money { get; private set; }
        public int damagePoints { get; private set; }
        public int armourPoints { get; private set; }
        public int healthPoints { get; private set; }
        public Equipment equipment { get; private set; }

        public HeroMemento(int money, int damagePoints, int armourPoints, int healthPoints, Equipment equipment) {
            this.money = money;
            this.damagePoints = damagePoints;
            this.armourPoints = armourPoints;
            this.healthPoints = healthPoints;
            this.equipment = equipment;
        }
    }

    class GameHistory {
        public Stack<HeroMemento> History { get; private set; }
        public GameHistory() {
            History = new Stack<HeroMemento>();
        }
    }
}

