﻿using myGamev1_0.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	class Pet {
		protected int damage { get; set; }
		public Pet(PictureBox pb) {
			pb.Image = Resources.pet;
		}
		public Pet() {

		}
		public virtual int dealDamage() {
			int damage = this.damage;
			return damage;
		}
	}
	class RarePet : Pet {
		public RarePet(PictureBox pb) {
			pb.Image = Resources.rpet;
			this.damage = this.damage * 2;
			Console.WriteLine("Rare pet");
		}

		public override int dealDamage() {
			return dealDamage();
		}
	}
	class DecoratorPet : Pet {
		protected Pet decoratedPet { get; set; }
		public DecoratorPet(Pet pet) {
			decoratedPet = pet;
		}

		public override int dealDamage() {
			return decoratedPet.dealDamage();
		}
	}

	class LegendaryPet : DecoratorPet {
		public LegendaryPet(Pet pet, PictureBox pb) : base(pet) {
			pb.Image = Resources.lpet;
		}

		public override int dealDamage() {
			base.dealDamage();
			Console.WriteLine("+100 gold");
			return 100;

			
		}
	}
}
