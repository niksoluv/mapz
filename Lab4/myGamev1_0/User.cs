﻿using myGamev1_0.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myGamev1_0 {
	public class User {
		public int money = 0;
		public int damagePoints = 4;
		public int armourPoints = 1;
		public int healthPoints = 20;
		public Equipment equipment;

		public User() {
			equipment = new Equipment();
			ShopFacade SF = new ShopFacade();
			equipment.weapon = SF.buyWeapon(100);
			equipment.armour = SF.buyArmour(10);
		}

		public HeroMemento SaveState() {
			return new HeroMemento(money, damagePoints, armourPoints, healthPoints, equipment);
		}

		public void RestoreState(HeroMemento memento) {
			money = memento.money;
			damagePoints = memento.damagePoints;
			armourPoints = memento.armourPoints;
			healthPoints = memento.healthPoints;
			equipment = memento.equipment;
		}

		public void dealDamage(Enemy enemy, TextBox Output, bool isFighting, Label moneyLabel) {
			if (isFighting) {
				int enemyDP = enemy.damagePoints;
				int enemyAR = enemy.armourPoints;
				int enemyHPO = enemy.healthPoints;
				int playerDP = damagePoints;
				int playerAR = armourPoints;
				int playerHPO = healthPoints;

				if (playerAR - enemyDP > 0) {
					Output.Text = Output.Text + "Damage taken : " +
						Convert.ToString(playerAR - enemyDP) + "\r\n";
					armourPoints = playerAR - enemyDP;
				}
				else {
					Output.Text = Output.Text + "Damage taken : " +
						Convert.ToString(enemyDP - playerAR) + "\r\n";
					armourPoints = 0;
					healthPoints -= enemyDP - playerAR;
				}

				if (enemyAR - playerDP > 0) {
					Output.Text = Output.Text + "Damage given : " +
						Convert.ToString(enemyAR - playerDP) + "\r\n";
					enemy.armourPoints = enemyAR - playerDP;
				}
				else {
					Output.Text = Output.Text + "Damage given : " +
						Convert.ToString(playerDP - enemyAR) + "\r\n";
					enemy.armourPoints = 0;
					enemy.healthPoints -= playerDP - enemyAR;
				}
				//form update
			}
		}

		public void Off() {
			Console.WriteLine("");
		}
	}

	public class Inventory {
		public List<Weapon> weapon = new List<Weapon>();
		public List<Armour> armour = new List<Armour>();
		public List<Potion> potions = new List<Potion>();
	}

	public class Equipment {
		public Weapon weapon;
		public Armour armour;
	}
}
