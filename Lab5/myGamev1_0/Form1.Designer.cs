﻿namespace myGamev1_0 {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.Start = new System.Windows.Forms.Button();
			this.heroSprite = new System.Windows.Forms.PictureBox();
			this.enemySprite = new System.Windows.Forms.PictureBox();
			this.petSprite = new System.Windows.Forms.PictureBox();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.shop = new System.Windows.Forms.Button();
			this.Output = new System.Windows.Forms.TextBox();
			this.movesLabel = new System.Windows.Forms.Label();
			this.moneyLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.userDMG = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.userAr = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.userHP = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.enemyHP = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.enemyAr = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.enemyDMG = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.heroSprite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.enemySprite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.petSprite)).BeginInit();
			this.SuspendLayout();
			// 
			// Start
			// 
			this.Start.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Start.Location = new System.Drawing.Point(713, 11);
			this.Start.Name = "Start";
			this.Start.Size = new System.Drawing.Size(75, 23);
			this.Start.TabIndex = 0;
			this.Start.Text = "Start";
			this.Start.UseVisualStyleBackColor = true;
			this.Start.Click += new System.EventHandler(this.button1_Click);
			// 
			// heroSprite
			// 
			this.heroSprite.Image = global::myGamev1_0.Properties.Resources.hero;
			this.heroSprite.Location = new System.Drawing.Point(83, 152);
			this.heroSprite.Name = "heroSprite";
			this.heroSprite.Size = new System.Drawing.Size(143, 213);
			this.heroSprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.heroSprite.TabIndex = 2;
			this.heroSprite.TabStop = false;
			// 
			// enemySprite
			// 
			this.enemySprite.Location = new System.Drawing.Point(532, 152);
			this.enemySprite.Name = "enemySprite";
			this.enemySprite.Size = new System.Drawing.Size(159, 213);
			this.enemySprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.enemySprite.TabIndex = 3;
			this.enemySprite.TabStop = false;
			// 
			// petSprite
			// 
			this.petSprite.Location = new System.Drawing.Point(37, 342);
			this.petSprite.Name = "petSprite";
			this.petSprite.Size = new System.Drawing.Size(24, 23);
			this.petSprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.petSprite.TabIndex = 4;
			this.petSprite.TabStop = false;
			// 
			// shop
			// 
			this.shop.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.shop.Location = new System.Drawing.Point(713, 40);
			this.shop.Name = "shop";
			this.shop.Size = new System.Drawing.Size(75, 23);
			this.shop.TabIndex = 6;
			this.shop.Text = "Shop";
			this.shop.UseVisualStyleBackColor = true;
			this.shop.Click += new System.EventHandler(this.shop_Click);
			// 
			// Output
			// 
			this.Output.Font = new System.Drawing.Font("Commons", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Output.Location = new System.Drawing.Point(13, 377);
			this.Output.Multiline = true;
			this.Output.Name = "Output";
			this.Output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.Output.Size = new System.Drawing.Size(775, 90);
			this.Output.TabIndex = 7;
			this.Output.MultilineChanged += new System.EventHandler(this.Output_MultilineChanged);
			this.Output.TextChanged += new System.EventHandler(this.Output_TextChanged);
			this.Output.ChangeUICues += new System.Windows.Forms.UICuesEventHandler(this.Output_ChangeUICues);
			// 
			// movesLabel
			// 
			this.movesLabel.AutoSize = true;
			this.movesLabel.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.movesLabel.Location = new System.Drawing.Point(429, 11);
			this.movesLabel.Name = "movesLabel";
			this.movesLabel.Size = new System.Drawing.Size(37, 12);
			this.movesLabel.TabIndex = 8;
			this.movesLabel.Text = "moves";
			this.movesLabel.Click += new System.EventHandler(this.label1_Click);
			// 
			// moneyLabel
			// 
			this.moneyLabel.AutoSize = true;
			this.moneyLabel.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.moneyLabel.Location = new System.Drawing.Point(653, 11);
			this.moneyLabel.Name = "moneyLabel";
			this.moneyLabel.Size = new System.Drawing.Size(38, 12);
			this.moneyLabel.TabIndex = 9;
			this.moneyLabel.Text = "money";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(365, 11);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 12);
			this.label1.TabIndex = 10;
			this.label1.Text = "moves left:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(606, 11);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(42, 12);
			this.label2.TabIndex = 11;
			this.label2.Text = "money:";
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.Location = new System.Drawing.Point(12, 10);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 12;
			this.button1.Text = "Attack";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click_1);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(229, 201);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(38, 14);
			this.label3.TabIndex = 15;
			this.label3.Text = "DMG:";
			// 
			// userDMG
			// 
			this.userDMG.AutoSize = true;
			this.userDMG.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.userDMG.Location = new System.Drawing.Point(269, 201);
			this.userDMG.Name = "userDMG";
			this.userDMG.Size = new System.Drawing.Size(31, 14);
			this.userDMG.TabIndex = 14;
			this.userDMG.Text = "dmg";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(229, 229);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(25, 14);
			this.label4.TabIndex = 17;
			this.label4.Text = "AR:";
			this.label4.Click += new System.EventHandler(this.label4_Click);
			// 
			// userAr
			// 
			this.userAr.AutoSize = true;
			this.userAr.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.userAr.Location = new System.Drawing.Point(269, 229);
			this.userAr.Name = "userAr";
			this.userAr.Size = new System.Drawing.Size(20, 14);
			this.userAr.TabIndex = 16;
			this.userAr.Text = "ar";
			this.userAr.Click += new System.EventHandler(this.label5_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(229, 170);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(25, 14);
			this.label5.TabIndex = 19;
			this.label5.Text = "HP:";
			// 
			// userHP
			// 
			this.userHP.AutoSize = true;
			this.userHP.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.userHP.Location = new System.Drawing.Point(269, 170);
			this.userHP.Name = "userHP";
			this.userHP.Size = new System.Drawing.Size(21, 14);
			this.userHP.TabIndex = 18;
			this.userHP.Text = "hp";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(697, 170);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(25, 14);
			this.label6.TabIndex = 25;
			this.label6.Text = "HP:";
			// 
			// enemyHP
			// 
			this.enemyHP.AutoSize = true;
			this.enemyHP.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.enemyHP.Location = new System.Drawing.Point(737, 170);
			this.enemyHP.Name = "enemyHP";
			this.enemyHP.Size = new System.Drawing.Size(21, 14);
			this.enemyHP.TabIndex = 24;
			this.enemyHP.Text = "hp";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(697, 229);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(25, 14);
			this.label8.TabIndex = 23;
			this.label8.Text = "AR:";
			// 
			// enemyAr
			// 
			this.enemyAr.AutoSize = true;
			this.enemyAr.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.enemyAr.Location = new System.Drawing.Point(737, 229);
			this.enemyAr.Name = "enemyAr";
			this.enemyAr.Size = new System.Drawing.Size(20, 14);
			this.enemyAr.TabIndex = 22;
			this.enemyAr.Text = "ar";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(697, 201);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(38, 14);
			this.label10.TabIndex = 21;
			this.label10.Text = "DMG:";
			// 
			// enemyDMG
			// 
			this.enemyDMG.AutoSize = true;
			this.enemyDMG.Font = new System.Drawing.Font("MelodBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.enemyDMG.Location = new System.Drawing.Point(737, 201);
			this.enemyDMG.Name = "enemyDMG";
			this.enemyDMG.Size = new System.Drawing.Size(31, 14);
			this.enemyDMG.TabIndex = 20;
			this.enemyDMG.Text = "dmg";
			// 
			// button2
			// 
			this.button2.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.Location = new System.Drawing.Point(94, 10);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 26;
			this.button2.Text = "Equipment";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button3.Location = new System.Drawing.Point(175, 10);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 27;
			this.button3.Text = "Inventory";
			this.button3.UseVisualStyleBackColor = true;
			// 
			// button4
			// 
			this.button4.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button4.Location = new System.Drawing.Point(13, 40);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 28;
			this.button4.Text = "Save";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Font = new System.Drawing.Font("Emmett", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button5.Location = new System.Drawing.Point(12, 69);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 29;
			this.button5.Text = "Load";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(800, 479);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.enemyHP);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.enemyAr);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.enemyDMG);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.userHP);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.userAr);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.userDMG);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.moneyLabel);
			this.Controls.Add(this.movesLabel);
			this.Controls.Add(this.Output);
			this.Controls.Add(this.shop);
			this.Controls.Add(this.petSprite);
			this.Controls.Add(this.enemySprite);
			this.Controls.Add(this.heroSprite);
			this.Controls.Add(this.Start);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.heroSprite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.enemySprite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.petSprite)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button Start;
		private System.Windows.Forms.PictureBox heroSprite;
		private System.Windows.Forms.PictureBox enemySprite;
		private System.Windows.Forms.PictureBox petSprite;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Button shop;
		private System.Windows.Forms.TextBox Output;
		private System.Windows.Forms.Label movesLabel;
		private System.Windows.Forms.Label moneyLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label userDMG;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label userAr;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label userHP;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label enemyHP;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label enemyAr;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label enemyDMG;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
	}
}

